{% from "asalae/map.jinja" import asalae with context %}
<?php
/**
 * Connexion à la base de données
 */
class DATABASE_CONFIG {

	var $default = array(
		'driver' => 'postgres',
		'persistent' => false,
		'host' => '{{ asalae.db.host }}',
		'port' => '{{ asalae.db.port }}',
		'login' => '{{ asalae.db.login }}',
		'password' => '{{ asalae.db.password }}',
		'database' => '{{ asalae.db.database }}',
		'prefix' => '',
		'encoding' => 'utf8'
	);


/**
 * ATTENTION, ne pas modifier les lignes ci-dessous : utilisation de as@lae en mode Multi-services d'archives
 */
	function __construct () {
		if (Configure::read('multiServicesArchives') && !empty($_SERVER['SERVER_NAME']))
			$this->default = array(
				'driver' => 'postgres',
				'persistent' => false,
				'host' => ASALAE_DBHOST,
				'port' => ASALAE_DBPORT,
				'login' => ASALAE_DBLOGIN,
				'password' => ASALAE_DBPASSWORD,
				'database' => ASALAE_DBDATABASE,
				'prefix' => '',
				'encoding' => 'utf8');
	}
}
?>
