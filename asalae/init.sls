{% from "asalae/map.jinja" import asalae with context %}

unzip:
  pkg:
    - installed

asalae_dist:
  archive:
    - name: /opt/asalae-dist
    - extracted
    - source: {{ asalae.dist.url }}
    - archive_format: zip
    - source_hash: {{ asalae.dist.hash }}
    - keep: True  # XXX
    - require:
      - pkg: unzip

apache2:
  pkg:
    - installed

/var/www/asalae:
  file.symlink:
    - target: /opt/asalae-dist/asalae
    - user: www-data
    - group: www-data
    - require:
      - pkg: apache2
      - archive: asalae_dist

/opt/asalae-dist/asalae:
  file.directory:
    - user: www-data
    - group: www-data
    - recurse:
      - user
      - group
    - require:
      - pkg: apache2
      - archive: asalae_dist

# Enable Debug for As@lae

patch:
  pkg:
    - installed

{% if asalae.debug %}
/opt/asalae-dist/asalae/app/config/core.php:
  file.patch:
    - source: salt://asalae/asalae-debug.patch
    - hash: md5=f6b8589ad331cf4651c250160a36d587
    - require:
      - pkg: patch
      - archive: asalae_dist
{% endif %}

#

{% for cfg in ('asalae.ini.php', 'database.php', 'types_connecteurs.ini.php') %}
/var/www/asalae/app/config/{{ cfg }}:
  file.managed:
    - source: salt://asalae/{{ cfg }}
    - template: jinja
    - require:
      - file: /var/www/asalae
{% endfor %}

php_dependencies:
  pkg.installed:
    - pkgs:
      - libapache2-mod-php5
      - php5-pgsql
      - php5-curl
      - php5-xsl
      - php-soap
    - require:
      - pkg: apache2

a2dismod deflate:
  cmd.run:
    - onlyif: test -f /etc/apache2/mods-enabled/deflate.load
    - require:
      - pkg: apache2

{% for apachemod in ('dav_fs', 'rewrite', 'ssl') %}
a2enmod {{ apachemod }}:
  cmd.run:
    - unless: test -f /etc/apache2/mods-enabled/{{ apachemod }}.load
    - require:
      - pkg: apache2
{% endfor %}

postgresql-client:
  pkg:
    - installed

#{% set psql_cmd = 'psql -h ' + asalae.db.host + ' ' + asalae.db.database + ' -U ' + asalae.db.login %}
#{{ psql_cmd }} -f /var/www/asalae/app/config/sql/postgresql/asalae_postgres_1.5.sql:
  #cmd.run:
    #- onlyif: {{ psql_cmd }} -c "SELECT * FROM pg_tables where tablename='acos';"
    #

/root/.pgpass:
  file.managed:
    - contents: {{ asalae.db.host }}:{{ asalae.db.port }}:{{ asalae.db.database }}:{{ asalae.db.user }}:{{ asalae.db.password }}
    - mode: 0600

# Patches for CakePHP which is not compatible with PHP5.4
/opt/asalae-dist/asalae/cake/bootstrap.php:
  file.patch:
    - source: salt://asalae/cakephp-bootstrap.patch
    - hash: md5=c916b7c01256d6c8db5643b96f998697
    - require:
      - pkg: patch
      - archive: asalae_dist

/opt/asalae-dist/asalae/cake/libs/debugger.php:
  file.patch:
    - source: salt://asalae/cakephp-debugger.patch
    - hash: md5=050cb655189e5598cbf7e3e3499b7159
    - require:
      - pkg: patch
      - archive: asalae_dist


/etc/apache2/sites-available/asalae:
  file.managed:
    - source: salt://asalae/apache-vhost.conf

a2ensite asalae:
  cmd.run:
    - unless: test -f /etc/apache2/sites-enabled/asalae
    - require:
      - file: /etc/apache2/sites-available/asalae

a2dissite 000-default:
  cmd.run:
    - onlyif: test -f /etc/apache2/sites-enabled/000-default
